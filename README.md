# adv

#### 介绍
基于Vaethink和uniapp的简单的广告机内容管理

#### 环境要求
| 环境要求 | Apache/Nginx | 基本运行环境 |
|---|--------------|--------|
| PHP版本 | 不能低于7.1 | 基本运行环境 |
| 数据库 | MySql | 基本运行环境 |
| file_uploads | 开启 | 将影响是否能够上传文件 |
| rewrite | 开启 | 将影响是否有伪静态，甚至是应用的运行 |


#### 安装教程
源码中的adv文件夹为广告机端源码，不需要上传服务器
 以lnmp为例
1.  解压源码至/home/wwwroot/你的项目文件夹内。
2.  对nginx进行配置，目录指向/home/wwwroot/你的项目文件夹/public
3.  重启或重新加载nginx
4.  访问 配置的项目域名/index.php/install/install/index.html进行安装，安装过程按照提示即可

#### 使用说明
##### 服务器端
后台地址：你的域名/index.php/admin/index/index.html
1.  进入后台点击应用->分类，添加分类组，添加一个即可
2.  点击内容添加内容组，内容组标识不能重复，分类组选择之前添加的分类组
3.  双击内容组进入添加内容
4.  内容中图片为广告机封面图片，具体内容可在最底下富文本编辑器中编辑。编辑完保存即可。
##### 广告机端
1.  使用Hbuilder X导入adv或者其他编辑器打开
2.  修改根目录vue.config.js中的urlPattern: new RegExp('^http://项目域名/')
3.  修改/src/pages/index/index.vue中的baseUrl为http://项目域名/index.php
4.  修改/src/pages/index/index.vue中的imageBaseUrl为http://项目域名
5.  Hbuilder X中在项目根目录右键选择外部命令->npm run build:h5或者在命令行直接输入npm run build:h5
6.  打包编译后的文件在/dist/build/h5目录内
7.  将打包编译后的文件上传至服务器/home/wwwroot/public/adv文件夹内，浏览器访问http://项目域名/adv即可
