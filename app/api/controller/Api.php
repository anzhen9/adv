<?php
declare (strict_types = 1);

namespace app\api\controller;
use app\api\BaseController;
use think\facade\Db;

class Api extends BaseController
{
    /**
     * 广告机列表
     */
    public function advMachineList()
    {
        $list = Db::name('content_group')
                ->field('id,name')
                ->select();
        return return_json(0,'',$list); 
    }

    public function advList(){
        $id = input("id");
        $list = Db::name('content')
        ->field('id,img,content,video')
        ->where('content_group_id',$id)
        ->order('sort desc')
        ->select();
        return return_json(0,'',$list); 
    }
}