SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `vaethink`
--

-- --------------------------------------------------------

--
-- 表的结构 `vae_admin`
--

CREATE TABLE `vae_admin` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `pwd` varchar(255) NOT NULL DEFAULT '',
  `salt` varchar(50) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1正常-1禁止登陆',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `last_login_time` int(11) NOT NULL DEFAULT '0',
  `last_login_ip` varchar(100) NOT NULL DEFAULT '',
  `nickname` varchar(255) DEFAULT '',
  `desc` text COMMENT '备注',
  `thumb` varchar(200) DEFAULT NULL,
  `groups` varchar(255) NOT NULL DEFAULT '' COMMENT '权限组,隔开'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员';

--
-- 转存表中的数据 `vae_admin`
--

INSERT INTO `vae_admin` (`id`, `username`, `pwd`, `salt`, `status`, `create_time`, `update_time`, `last_login_time`, `last_login_ip`, `nickname`, `desc`, `thumb`, `groups`) VALUES
(1, 'admin', '83aa6b2e186924caa95d966422e932be', 'jtu7nXeNgkTDVd4wSBpL', 1, 1591015211, 1591015211, 1605840229, '192.168.0.95', 'Admin', NULL, '/static/admin_static/images/vae.jpg', '');

-- --------------------------------------------------------

--
-- 表的结构 `vae_admin_group`
--

CREATE TABLE `vae_admin_group` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT '1',
  `rules` text COMMENT '用户组拥有的规则id， 多个规则","隔开',
  `desc` text COMMENT '备注',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限分组';

-- --------------------------------------------------------

--
-- 表的结构 `vae_admin_rule`
--

CREATE TABLE `vae_admin_rule` (
  `id` int(11) UNSIGNED NOT NULL,
  `pid` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `src` varchar(255) NOT NULL DEFAULT '' COMMENT '规则',
  `title` varchar(255) NOT NULL DEFAULT '',
  `is_menu` int(1) NOT NULL DEFAULT '1' COMMENT '1是菜单2不是',
  `font_family` varchar(50) DEFAULT '' COMMENT '图标来源',
  `icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序，越大越靠前',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL COMMENT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限节点';

--
-- 转存表中的数据 `vae_admin_rule`
--

INSERT INTO `vae_admin_rule` (`id`, `pid`, `src`, `title`, `is_menu`, `font_family`, `icon`, `sort`, `create_time`, `update_time`) VALUES
(1, 0, '', '系统', 1, '', 'layui-icon-windows', 0, 0, 0),
(2, 1, 'menu/index', '菜单', 1, '', 'layui-icon-tree', 0, 0, 0),
(3, 2, 'menu/add', '添加菜单', 0, '', '', 0, 0, 0),
(4, 2, 'menu/edit', '修改菜单', 0, '', '', 0, 0, 0),
(5, 2, 'menu/delete', '删除菜单', 0, '', '', 0, 0, 0),
(6, 14, 'admin/index', '管理员', 1, '', 'layui-icon-friends', 0, 0, 0),
(7, 6, 'admin/add', '添加管理员', 0, '', '', 0, 0, 0),
(8, 6, 'admin/edit', '修改管理员', 0, '', '', 0, 0, 0),
(9, 6, 'admin/delete', '删除管理员', 0, '', '', 0, 0, 0),
(10, 14, 'group/index', '管理组', 1, '', 'layui-icon-user', 0, 0, 0),
(11, 10, 'group/add', '添加权限组', 0, '', '', 0, 0, 0),
(12, 10, 'group/edit', '修改权限组', 0, '', '', 0, 0, 0),
(13, 10, 'group/delete', '删除权限组', 0, '', '', 0, 0, 0),
(14, 1, '', '安全', 1, '', 'layui-icon-auz', -1, 0, 0),
(15, 1, 'conf/index', '配置', 1, '', 'layui-icon-set', 0, 0, 0),
(16, 15, 'conf/confsubmit', '提交配置', 0, '', '', 0, 0, 0),
(17, 0, '', '应用', 1, '', 'layui-icon-app', 0, 0, 0),
(18, 17, 'nav/index', '导航', 1, '', 'layui-icon-component', 0, 0, 0),
(19, 18, 'nav/addgroup', '添加导航组', 0, '', '', 0, 0, 0),
(20, 18, 'nav/editgroup', '修改导航组', 0, '', '', 0, 0, 0),
(21, 18, 'nav/deletegroup', '删除导航组', 0, '', '', 0, 0, 0),
(22, 18, 'nav/navindex', '导航集', 0, '', '', 0, 0, 0),
(23, 18, 'nav/addnav', '添加导航', 0, '', '', 0, 0, 0),
(24, 18, 'nav/editnav', '修改导航', 0, '', '', 0, 0, 0),
(25, 18, 'nav/deletenav', '删除导航', 0, '', '', 0, 0, 0),
(26, 1, 'route/index', '路由', 1, '', 'layui-icon-link', 0, 0, 0),
(27, 26, 'route/add', '添加美化', 0, '', '', 0, 0, 0),
(28, 26, 'route/edit', '修改美化', 0, '', '', 0, 0, 0),
(29, 26, 'route/delete', '删除美化', 0, '', '', 0, 0, 0),
(31, 17, 'slide/index', '轮播', 1, '', 'layui-icon-carousel', 0, 0, 0),
(32, 17, 'cate/index', '分类', 1, '', 'layui-icon-tree', 0, 0, 0),
(34, 31, 'slide/addgroup', '添加轮播组', 0, '', '', 0, 0, 0),
(35, 31, 'slide/editgroup', '修改轮播组', 0, '', '', 0, 0, 0),
(36, 31, 'slide/deletegroup', '删除轮播组', 0, '', '', 0, 0, 0),
(37, 31, 'slide/slideindex', '轮播集', 0, '', '', 0, 0, 0),
(38, 31, 'slide/addslide', '添加轮播图', 0, '', '', 0, 0, 0),
(39, 31, 'slide/editslide', '修改轮播图', 0, '', '', 0, 0, 0),
(40, 31, 'slide/deleteslide', '删除轮播图', 0, '', '', 0, 0, 0),
(41, 32, 'cate/addgroup', '添加分类组', 0, '', '', 0, 0, 0),
(42, 32, 'cate/editgroup', '修改分类组', 0, '', '', 0, 0, 0),
(43, 32, 'cate/deletegroup', '删除分类组', 0, '', '', 0, 0, 0),
(44, 32, 'cate/cateindex', '分类集', 0, '', '', 0, 0, 0),
(45, 32, 'cate/addcate', '添加分类', 0, '', '', 0, 0, 0),
(46, 32, 'cate/editcate', '修改分类', 0, '', '', 0, 0, 0),
(47, 32, 'cate/deletecate', '删除分类', 0, '', '', 0, 0, 0),
(48, 17, 'content/index', '内容', 1, '', 'layui-icon-template-1', 0, 0, 0),
(49, 48, 'content/addgroup', '添加内容组', 0, '', '', 0, 0, 0),
(50, 48, 'content/editgroup', '修改内容组', 0, '', '', 0, 0, 0),
(51, 48, 'content/deletegroup', '删除内容组', 0, '', '', 0, 0, 0),
(52, 48, 'content/addcontent', '添加内容', 0, '', '', 0, 0, 0),
(53, 48, 'content/editcontent', '修改内容', 0, '', '', 0, 0, 0),
(54, 48, 'content/deletecontent', '删除内容', 0, '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `vae_cate`
--

CREATE TABLE `vae_cate` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `pid` int(11) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1正常0下架',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序，值越大越靠前',
  `cate_group_id` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类';

-- --------------------------------------------------------

--
-- 表的结构 `vae_cate_group`
--

CREATE TABLE `vae_cate_group` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `key` varchar(50) NOT NULL,
  `desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类分组';


-- --------------------------------------------------------

--
-- 表的结构 `vae_content`
--

CREATE TABLE `vae_content` (
  `id` int(11) UNSIGNED NOT NULL,
  `img` text COMMENT '图片，多图用,隔开',
  `video` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1正常0下架',
  `title` varchar(255) NOT NULL,
  `desc` varchar(500) DEFAULT NULL COMMENT '概要',
  `content` text COMMENT '详情',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序，值越大越靠前',
  `content_group_id` int(11) NOT NULL DEFAULT '0',
  `cate_id` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='内容';

-- --------------------------------------------------------

--
-- 表的结构 `vae_content_group`
--

CREATE TABLE `vae_content_group` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `key` varchar(50) NOT NULL,
  `cate_group_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类组id',
  `desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='内容分组';


-- --------------------------------------------------------

--
-- 表的结构 `vae_nav`
--

CREATE TABLE `vae_nav` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `src` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1正常0下架',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序，值越大越靠前',
  `nav_group_id` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='导航';

-- --------------------------------------------------------

--
-- 表的结构 `vae_nav_group`
--

CREATE TABLE `vae_nav_group` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `key` varchar(50) NOT NULL,
  `desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='导航分组';

-- --------------------------------------------------------

--
-- 表的结构 `vae_route`
--

CREATE TABLE `vae_route` (
  `id` int(11) UNSIGNED NOT NULL,
  `module` varchar(255) NOT NULL DEFAULT '' COMMENT '应用名',
  `full_url` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1启用-1禁用',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='路由设置';

-- --------------------------------------------------------

--
-- 表的结构 `vae_slide`
--

CREATE TABLE `vae_slide` (
  `id` int(11) UNSIGNED NOT NULL,
  `icon` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1正常0下架',
  `src` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序，值越大越靠前',
  `slide_group_id` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='轮播';

-- --------------------------------------------------------

--
-- 表的结构 `vae_slide_group`
--

CREATE TABLE `vae_slide_group` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `key` varchar(50) NOT NULL,
  `desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='轮播分组';

--
-- 转储表的索引
--

--
-- 表的索引 `vae_admin`
--
ALTER TABLE `vae_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`id`,`username`) USING BTREE;

--
-- 表的索引 `vae_admin_group`
--
ALTER TABLE `vae_admin_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`) USING BTREE;

--
-- 表的索引 `vae_admin_rule`
--
ALTER TABLE `vae_admin_rule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`src`) USING BTREE;

--
-- 表的索引 `vae_cate`
--
ALTER TABLE `vae_cate`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `vae_cate_group`
--
ALTER TABLE `vae_cate_group`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `vae_content`
--
ALTER TABLE `vae_content`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `vae_content_group`
--
ALTER TABLE `vae_content_group`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `vae_nav`
--
ALTER TABLE `vae_nav`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `vae_nav_group`
--
ALTER TABLE `vae_nav_group`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `vae_route`
--
ALTER TABLE `vae_route`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `vae_slide`
--
ALTER TABLE `vae_slide`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `vae_slide_group`
--
ALTER TABLE `vae_slide_group`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `vae_admin`
--
ALTER TABLE `vae_admin`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `vae_admin_group`
--
ALTER TABLE `vae_admin_group`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `vae_admin_rule`
--
ALTER TABLE `vae_admin_rule`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- 使用表AUTO_INCREMENT `vae_cate`
--
ALTER TABLE `vae_cate`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `vae_cate_group`
--
ALTER TABLE `vae_cate_group`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `vae_content`
--
ALTER TABLE `vae_content`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- 使用表AUTO_INCREMENT `vae_content_group`
--
ALTER TABLE `vae_content_group`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- 使用表AUTO_INCREMENT `vae_nav`
--
ALTER TABLE `vae_nav`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `vae_nav_group`
--
ALTER TABLE `vae_nav_group`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `vae_route`
--
ALTER TABLE `vae_route`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `vae_slide`
--
ALTER TABLE `vae_slide`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `vae_slide_group`
--
ALTER TABLE `vae_slide_group`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
