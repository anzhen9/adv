module.exports = {
	pwa: {
		manifestOptions: {
			"name": "数字屏",
			"short_name": "数字屏",
			"theme_color": "#2e85ff",
			"icons": [{
				src: "./static/img/icons/android-chrome-192x192.png",
				sizes: "192x192",
				type: "image/png"
			}, {
				src: "./static/img/icons/android-chrome-512x512.png",
				sizes: "512x512",
				type: "image/png"
			}, {
				src: "./static/img/icons/android-chrome-maskable-192x192.png",
				sizes: "192x192",
				type: "image/png",
				purpose: "maskable"
			}, {
				src: "./static/img/icons/android-chrome-maskable-512x512.png",
				sizes: "512x512",
				type: "image/png",
				purpose: "maskable"
			}],
			"start_url": ".",
			"display": "standalone",
			"background_color": "#FFFFFF"
		},
		name: '数字屏',
		themeColor: '#2e85ff',
		msTileColor: '#ff5331',
		appleMobileWebAppCapable: 'yes',
		appleMobileWebAppStatusBarStyle: 'black-translucent',
		iconPaths: {
			favicon32: './static/img/icons/favicon-32x32.png',
			favicon16: './static/img/icons/favicon-16x16.png',
			appleTouchIcon: './static/img/icons/apple-touch-icon-152x152.png',
			maskIcon: './static/img/icons/safari-pinned-tab.svg',
			msTileImage: './static/img/icons/msapplication-icon-144x144.png'
		},
		workboxPluginMode: 'GenerateSW',
		// workboxOptions: {
		// 	skipWaiting:true,
		// 	clientsClaim: true,
		// 	navigateFallback: './index.html',
		// 	runtimeCaching: [{
		// 		urlPattern: new RegExp('^http://adv.anzhen9.com/'),
		// 		handler: 'networkFirst',
		// 		options: {
		// 			networkTimeoutSeconds: 20,
		// 			cacheName: 'api-cache',
		// 			cacheableResponse: {
		// 				statuses: [0, 200]
		// 			}
		// 		}
		// 	}]
		// }
	}
}
